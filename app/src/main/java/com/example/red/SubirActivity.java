package com.example.red;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.red.network.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class SubirActivity extends AppCompatActivity {

    public static final String WEB = "http://192.168.103.113/acceso/upload.php";
    @BindView(R.id.edFichero)
    EditText edFichero;
    @BindView(R.id.tvInfo)
    TextView tvInfo;

    @OnClick(R.id.btSubir)
    public void subir(View view){
        String nombre = tvInfo.getText().toString();

        final ProgressDialog progreso = new ProgressDialog(this);
        File fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), nombre);
        boolean existe = true;

        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpdate", fichero);
        } catch (FileNotFoundException e) {
            tvInfo.setText("Error: " + e.getMessage() + "\n" + Environment.getExternalStorageDirectory().getAbsolutePath());
            existe = false;
        }
        if (existe){
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    progreso.setCancelable(true);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progreso.dismiss();
                    tvInfo.setText("Error " + statusCode + "\n" + throwable.getMessage() );
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progreso.dismiss();
                    tvInfo.setText("Resultado: " + responseString);
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir);

        ButterKnife.bind(this);
    }
}
