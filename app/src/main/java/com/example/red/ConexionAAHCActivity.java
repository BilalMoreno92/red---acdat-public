package com.example.red;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import com.example.red.network.RestClient;
import com.loopj.android.http.TextHttpResponseHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class ConexionAAHCActivity extends AppCompatActivity {

    private static final int REQUEST_CONNECT = 1;
    @BindView(R.id.edURL)
    EditText edURL;
    @BindView(R.id.wvContenido)
    WebView wvContenido;
    @BindView(R.id.tvTime)
    TextView tvTiempo;
    long inicio, fin;
    String url;
    private ProgressDialog progreso;

    @OnClick(R.id.btIr)
    public void conectar(View view) {
        hideSoftKeyboard();
        url = edURL.getText().toString();
        inicio = System.currentTimeMillis();
        progreso = new ProgressDialog(this);
        RestClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(true);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                fin = System.currentTimeMillis();
                wvContenido.loadDataWithBaseURL(url, "Error " + statusCode + ": " + throwable.getMessage(), "text/html", "UTF-8", null);
                tvTiempo.setText("Tiempo en cargar: " + String.valueOf(fin - inicio));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                progreso.dismiss();
                fin = System.currentTimeMillis();
                wvContenido.loadDataWithBaseURL(url, "Error " + statusCode + ": " + throwable.getMessage(), "text/html", "UTF-8", null);
                tvTiempo.setText("Tiempo en cargar: " + String.valueOf(fin - inicio));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progreso.dismiss();
                fin = System.currentTimeMillis();
                wvContenido.loadDataWithBaseURL(url, responseString, "text/html", "UTF-8", null);
                tvTiempo.setText("Tiempo en cargar: " + String.valueOf(fin - inicio));
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conexion_aahc);

        ButterKnife.bind(this);
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }

}
