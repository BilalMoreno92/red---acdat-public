package com.example.red;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.red.model.Resultado;
import com.example.red.network.Conexion;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConexionAsincronaActivity extends AppCompatActivity {

    private static final int REQUEST_CONNECT = 1;
    @BindView(R.id.edURL)
    EditText edURL;
    @BindView(R.id.wvContenido)
    WebView wvContenido;
    @BindView(R.id.tvTime)
    TextView tvTiempo;
    long inicio, fin;
    String url;
    TareaAsincrona tareaAsincrona;

    @OnClick(R.id.btIr)
    public void conectar(View view) {
        if (comprobarPermiso()) {
            url = edURL.getText().toString();
            descarga(url);
        }
        else
            mostrarError();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conexion_asincrona);

        ButterKnife.bind(this);
    }

    private boolean comprobarPermiso() {
        String permiso = Manifest.permission.INTERNET;
        boolean concedido = false;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            // pedir los permisos necesarios, porque no están concedidos
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permiso)) {
                concedido = false;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_CONNECT);
                // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
            }
        } else {
            concedido = true;
        }
        return concedido;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        String permiso = Manifest.permission.INTERNET;
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED)
                // permiso concedido
                descarga(url);
            else
                // no hay permiso
                mostrarError();
    }

    private void descarga(String url) {
        inicio = System.currentTimeMillis();
        tareaAsincrona = new TareaAsincrona(this);
        tareaAsincrona.execute(url);
        tvTiempo.setText("Descargando la página...");
    }

    private void mostrarError() {
        Toast.makeText(this, "No hay permiso para conectarse a Internet", Toast.LENGTH_SHORT).show();
    }

    public class TareaAsincrona extends AsyncTask<String, Void, Resultado> {
        private ProgressDialog progreso;
        private Context context;

        public TareaAsincrona(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(true);
            progreso.setOnCancelListener(new DialogInterface.OnCancelListener(){
                public void onCancel(DialogInterface dialog){
                    TareaAsincrona.this.cancel(true);
                }
            });
            progreso.show();
        }

        @Override
        protected Resultado doInBackground(String... cadena) {
            Resultado resultado;

            try {
                resultado = Conexion.conectarJava(cadena[0]);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                resultado = new Resultado();
                resultado.setCodigo(false);
                resultado.setMensaje(e.getMessage());
            }
            return resultado;
        }

        @Override
        protected void onPostExecute(Resultado resultado) {
            progreso.dismiss();
            fin = System.currentTimeMillis();
            if (resultado.getCodigo()){
                wvContenido.loadDataWithBaseURL(url, resultado.getContenido(), "text/html", "UTF-8", null);
            } else {
                wvContenido.loadDataWithBaseURL(url, resultado.getMensaje(), "text/html", "UTF-8", null);
            }
            tvTiempo.setText("Tiempo: " + String.valueOf(fin - inicio));
        }

        @Override
        protected void onCancelled() {
            progreso.dismiss();
            // mostrar cancelación
            //Toast.makeText(context, "Cancelado", Toast.LENGTH_SHORT).show();
        }
    }

}
