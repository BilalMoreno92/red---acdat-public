package com.example.red;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.red.network.RestClient;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import okhttp3.OkHttpClient;

public class DescargarActivity extends AppCompatActivity {

    private static final int REQUEST_CONNECT = 1;

    String url;

    @BindView(R.id.edURL)
    EditText edURL;

    @BindView(R.id.ivImage)
    ImageView ivImage;

    @BindView(R.id.tvFichero)
    TextView tvFichero;

    @OnClick(R.id.btImagen)
    public void descargarImagen(View view) {
        url = edURL.getText().toString();
        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(client))
                .build();
        picasso.get()
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder_error)
                .into(ivImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplicationContext(), "Descarga OK", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Exception e) {
                        Toast.makeText(getApplicationContext(), "Error al descargar:\n" + url, Toast.LENGTH_SHORT).show();
                    }
                });
        ivImage.setVisibility(View.VISIBLE);
        tvFichero.setVisibility(View.GONE);

    }

    @OnClick(R.id.btFichero)
    public void descargarFichero(View view) {
        if (comprobarPermiso()) {
            descargar(url);
            ivImage.setVisibility(View.GONE);
            tvFichero.setVisibility(View.VISIBLE);
        } else {
            mostrarMensaje("No hay permiso de escritura");
        }
    }

    private void descargar(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        RestClient.get(url, new FileAsyncHttpResponseHandler(fichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(true);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                mostrarMensaje("Error: " + statusCode + "\n" + throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                mostrarMensaje("Fichero descargado OK:\n" + file.getAbsolutePath());
            }
        });
    }

    private void mostrarMensaje(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descargar);

        Toast.makeText(this, Environment.getExternalStorageDirectory().getAbsolutePath(), Toast.LENGTH_LONG).show();

        ButterKnife.bind(this);
    }

    private boolean comprobarPermiso() {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        boolean concedido = false;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_CONNECT);
            // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
        } else {
            concedido = true;
        }
        return concedido;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED)
                // permiso concedido
                descargar(url);
            else
                // no hay permiso
                mostrarMensaje("No se ha concedido el permiso");
    }

}
