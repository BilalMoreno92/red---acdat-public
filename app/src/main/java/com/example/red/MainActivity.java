package com.example.red;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.btAsincrona, R.id.btAAHC, R.id.btSubir, R.id.btDescargar})
    public void openActivity(Button button){
        Intent intent = null;
        switch (button.getId()){
            case R.id.btAsincrona:
                intent = new Intent(this, ConexionAsincronaActivity.class);
                break;
            case R.id.btAAHC:
                intent = new Intent(this, ConexionAAHCActivity.class);
                break;
            case R.id.btSubir:
                intent = new Intent(this, SubirActivity.class);
                break;
            case R.id.btDescargar:
                intent = new Intent(this, DescargarActivity.class);
                break;
        }
        startActivity(intent);
    }
}
